

class Funcions {

    public static String inverteix(String paraula){
        //..
        char[] lletres = paraula.toCharArray();
        String resposta = "";
        for(int i = lletres.length-1; i>=0; i=i-1){
            resposta = resposta+lletres[i];
        }
        return resposta;
    }

    public static String inverteix2(String paraula){
        //..
        StringBuilder resposta = new StringBuilder();
        char[] lletres = paraula.toCharArray();
        for(int i = lletres.length-1; i>=0; i=i-1){
            resposta.append(lletres[i]);
        }
        return resposta.toString();
    }


}