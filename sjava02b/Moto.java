

class Moto {
    private String marca;
    private String modelo;
    private int cv;
    private double precio;

    public Moto(String marca, String modelo, int cv, double precio){
        this.marca = marca;
        this.modelo = modelo;
        this.cv = cv;
        this.precio = precio;
    }
    public String getMarca(){
        return this.marca;
    }
    public String getModelo(){
        return this.modelo;
    }
    public int getCv(){
        return this.cv;
    }
    public double getPrecio(){
        return this.precio;
    }
}