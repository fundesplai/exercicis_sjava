
public class MotoUtil {

    public static void informe(Moto[] motos){

        Moto mesCara = MotoUtil.masCara(motos);
        Moto mesPotent = MotoUtil.masPotente(motos);
        System.out.println("La mas cara es " + mesCara.getModelo() + " ("+ mesCara.getPrecio()+"eur).");
        System.out.println("La mas potente es " + mesPotent.getModelo() + " ("+ mesPotent.getCv()+"cv).");
        
        double total = 0;
        for(Moto m : motos){
            total = total + m.getPrecio();
        }
        System.out.println("El precio total es de " + total + "EUR.");

    };
    public static void compara(Moto moto1, Moto moto2){

    };
    public static Moto masPotente(Moto[] motos){
        Moto mesPotent = motos[0];
        for(Moto m : motos){
            if (m.getCv() > mesPotent.getCv()){
                mesPotent = m;
            }
        }
        return mesPotent;
    };
    public static Moto masCara(Moto[] motos){
        Moto mesCara = motos[0];
        for(Moto m : motos){
            if (m.getPrecio() > mesCara.getPrecio()){
                mesCara = m;
            }
        }
        return mesCara;
    };
    

}