
import java.util.Random;

class Biblioteca {

    public static String[] diesSetmana(String idioma){
        if (idioma.equals("ca")){
            return new String[] {"dilluns", "dimarts", "dimecres", "dijous", "divendres", "dissabte", "diumenge"};
        }
        if (idioma.equals("es")){
            return new String[] {"lunes", "martes", "miércoles", "jueves", "viernes", "sábado", "domingo"};
        } 
        return null;
    }

    // retorna string invertit i passat a minuscules
    public static String inverteix(String paraula){
        StringBuilder resposta = new StringBuilder();
        char[] lletres = paraula.toCharArray();
        for(int i = lletres.length-1; i>=0; i=i-1){
            resposta.append(lletres[i]);
        }
        return resposta.toString().toLowerCase();
    }

    
public static String password(int llarg){
    String s = "_;${[]}()abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    char[] ca = s.toCharArray();
    String pass = "";
    Random random = new Random();
    for(int i = 0; i<llarg; i++){
        int posicioRandom = random.nextInt(ca.length);
        char lletraRandom = ca[posicioRandom];
        pass = pass + lletraRandom;
    }
    return pass;
}

}

