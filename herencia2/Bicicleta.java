public class Bicicleta extends Vehicle  implements Pesable {

    public int pes;
    public String tipus;

    public Bicicleta(String marca, String model, char energia, int pes, String tipus){
        super(marca, model, energia, 2, 1);
        this.pes = pes;
        this.tipus = tipus;
    }
    
    public int quantPesa(){
        return this.pes;
    }

}