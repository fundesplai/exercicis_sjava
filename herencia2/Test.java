
class Test {

    public static void main(String[] args) {

        Vehicle v1 = new Moto("Honda", "CB125", 't', 125);
        Vehicle v2 = new Bicicleta("Trek", "Caliber", 'm', 10, "normal");
        Vehicle v3 = new Patinet("Xiaomi", "X", 'e', 6);

        descriu(v1);
        descriu(v2);
        descriu(v3);
    }

    public static void descriu(Vehicle v){
        System.out.println("Marca: "+v.marca);
        System.out.println("Model: "+v.model);
        if (v instanceof Pesable){
            Pesable p = (Pesable) v;
            System.out.println("Pes: "+p.quantPesa());
        }
        if (v instanceof Solitari){
            System.out.println("Monoplaça");
        }
    }


}
