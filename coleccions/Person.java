

class Person implements Comparable {
    String nom;
    int edat;

    public Person(String n, int e){
        this.nom = n;
        this.edat = e;
    }

    @Override
    public String toString(){
        return this.nom + " te "+ this.edat+" anys";
    }

    @Override
    public int hashCode(){
        return this.edat;
    }

    public boolean equals(Object x){
        Person xx = (Person) x;
        if (xx.nom.equals(this.nom)){
            return true;
        } else {
            return false;
        }
    }

    public int compareTo(Object x){
        Person xx = (Person) x;
        return xx.edat - this.edat;
    }

}