
import java.util.*;


class Test {

    public static void main(String[] args) {

        // Collection<String> coleccio = new ArrayList<String>();

        // coleccio.add("hola");
        // coleccio.add("que");
        // coleccio.add("tal");

        // for (String x : coleccio){
        //     System.out.println(x);
        // }

        Set<Person> cpersones = new TreeSet<Person>();

        Person p1 = new Person("pepe", 20);
        Person p2 = new Person("marta", 22);
        Person p3 = new Person("ester", 15);
        Person p4 = new Person("pepe", 25);
        cpersones.add(p1);
        cpersones.add(p2);
        cpersones.add(p3);
        cpersones.add(p4);

        for (Person x : cpersones){
            System.out.println(x);
        }




    }

}